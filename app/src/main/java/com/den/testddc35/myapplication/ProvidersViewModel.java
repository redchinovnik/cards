package com.den.testddc35.myapplication;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.den.testddc35.myapplication.data.ProvidersWrapper;
import com.den.testddc35.myapplication.data_sources.ProvidersRepositary;

import javax.inject.Inject;

public class ProvidersViewModel extends ViewModel {
    @Inject
    ProvidersRepositary providersRepositary;
    private LiveData<ProvidersWrapper> receivedProviders;

    public ProvidersViewModel() {
        App.getAppComponent().injectProvidersViewModel(this);
    }


    public LiveData<ProvidersWrapper> getProviders() {
        if (receivedProviders == null) {
            receivedProviders = providersRepositary.getProviders();
            return receivedProviders;
        }
        if (receivedProviders.getValue().getStatus() == ProvidersWrapper.Status.ERROR)
            receivedProviders = providersRepositary.getProviders();

        return receivedProviders;
    }

}
