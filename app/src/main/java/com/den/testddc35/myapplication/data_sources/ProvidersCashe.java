package com.den.testddc35.myapplication.data_sources;

import android.arch.lifecycle.LiveData;

import com.den.testddc35.myapplication.data.Provider;
import com.den.testddc35.myapplication.data.ProvidersResponse;
import com.den.testddc35.myapplication.data.ProvidersWrapper;

import java.util.List;

public interface ProvidersCashe {
    LiveData<ProvidersWrapper> getProviders();

    void addProviders(ProvidersResponse providers);
}
