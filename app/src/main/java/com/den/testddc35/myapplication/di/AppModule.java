package com.den.testddc35.myapplication.di;

import android.content.Context;

import com.den.testddc35.myapplication.data_sources.ProvidersRESTService;
import com.den.testddc35.myapplication.data_sources.ProvidersRepositary;
import com.den.testddc35.myapplication.data_sources.ProvidersRepositaryImpl;
import com.den.testddc35.myapplication.data_sources.ProvidersCahseImpl;
import com.den.testddc35.myapplication.data_sources.ProvidersCashe;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {
    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public ProvidersRESTService provideProvidersService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(/*context.getString(R.string.BASE_URL)*/"http://91.240.86.243:8883")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(ProvidersRESTService.class);
    }

    @Provides
    @Singleton
    public ProvidersCashe provideCashe() {
        return new ProvidersCahseImpl(context);
    }

    @Provides
    @Singleton
    public ProvidersRepositary provideRepositary(ProvidersRESTService providersService, ProvidersCashe providersCashe) {
        return new ProvidersRepositaryImpl(providersService,providersCashe);
    }
}
