package com.den.testddc35.myapplication.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.den.testddc35.myapplication.data.Provider;

import java.util.List;

public abstract class ProvidersAdapterAbstract extends RecyclerView.Adapter<ProvidersAdapterAbstract.ViewHolder> {
    protected List<Provider> items;


    public void setProviders(List<Provider> providers) {
        this.items = providers;
        notifyDataSetChanged();
    }

    public List<Provider> getItems() {
        return items;
    }

    public static abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
