package com.den.testddc35.myapplication.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.den.testddc35.myapplication.R;

public class ProvidersAdapter2 extends ProvidersAdapterAbstract {


    public static class PViewHolder extends ProvidersAdapterAbstract.ViewHolder {
        public TextView mTextView;
        public RecyclerView recyclerView;

        public PViewHolder(Context context, View v) {
            super(v);
            mTextView = v.findViewById(R.id.tv_title);
            recyclerView = v.findViewById(R.id.recycler_view);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
        }
    }




    @Override
    public ProvidersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_provider_2, parent, false);
        ViewHolder vh = new PViewHolder(parent.getContext(), v);
        return vh;
    }




    @Override
    public void onBindViewHolder(ViewHolder holder1, int position) {
        PViewHolder holder=(PViewHolder) holder1;
        holder.mTextView.setText(items.get(position).getTitle());
        CardsAdapter2 cardsAdapter = (CardsAdapter2) holder.recyclerView.getAdapter();
        if (cardsAdapter == null) {
            cardsAdapter = new CardsAdapter2();
            holder.recyclerView.setAdapter(cardsAdapter);
        }
        cardsAdapter.setCards(items.get(position).getGiftCards());
        holder.recyclerView.post(new Runnable() {
            @Override
            public void run() {
                holder.recyclerView.scrollToPosition(0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}