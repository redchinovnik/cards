package com.den.testddc35.myapplication;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;

import com.den.testddc35.myapplication.data.GiftCard;

public class NavigationController {

    public static void viewCard(View view, GiftCard card,int color) {
        Activity activity = (Activity) view.getContext();
        Intent intent = ViewCardActivity.getCallIntent(activity, card, color);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String transition_name = String.valueOf(System.currentTimeMillis());
            view.setTransitionName(transition_name);
            intent.putExtra(ViewCardActivity.TRANSITION_NAME,transition_name);
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(activity, view, transition_name);

            activity.startActivity(intent, options.toBundle());
        } else
            activity.startActivity(intent);
    }

}
