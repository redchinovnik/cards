package com.den.testddc35.myapplication.di;

import com.den.testddc35.myapplication.MainActivity;
import com.den.testddc35.myapplication.ProvidersViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {
    void injectMainActivity(MainActivity activity);
    void injectProvidersViewModel(ProvidersViewModel model);
}
