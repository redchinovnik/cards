package com.den.testddc35.myapplication;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.graphics.Palette;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Locale;

public class BindingsAdapters {

    @BindingAdapter({"coins"})
    public static void setCoins(TextView view, String coins) {
        String curenncy = coins.substring(0, 1);
        float c = Float.parseFloat(coins.substring(1));
        view.setText(curenncy + String.format("%.2f", c));
    }

    @BindingAdapter({"imageUrl", "view"})
    public static void setImageUrl(ImageView view, String url, View bkgView) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                view.setAlpha(0f);
                view.setImageBitmap(bitmap);
                view.animate().setDuration(250).alpha(1f).start();
                if (bkgView != null)
                    Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                        public void onGenerated(Palette p) {
                            Palette.Swatch vibrant = p.getVibrantSwatch();
                            int color = view.getContext().getResources().getColor(R.color.colorPrimaryLight);
                            int oldColor = 0;
                            Drawable background = bkgView.getBackground();
                            if (background instanceof ColorDrawable)
                                oldColor = ((ColorDrawable) background).getColor();
                            else
                                return;

                            if (vibrant != null) {
                                color = vibrant.getRgb();
                            }
                            final ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(bkgView,
                                    "backgroundColor",
                                    new ArgbEvaluator(),
                                    oldColor,
                                    color);
                            backgroundColorAnimator.setDuration(250);
                            backgroundColorAnimator.start();
                        }
                    });
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        view.setTag(target);
        Picasso.with(view.getContext()).load(url).into(target);


    }

    @BindingAdapter("fadeVisible")
    public static void setFadeVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.GONE);
                    }
                });
            }
        }
    }
}
