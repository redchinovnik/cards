package com.den.testddc35.myapplication.data_sources;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.den.testddc35.myapplication.data.ProvidersResponse;
import com.den.testddc35.myapplication.data.ProvidersWrapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProvidersRepositaryImpl implements ProvidersRepositary {
    private ProvidersRESTService providersService;
    private ProvidersCashe providersCashe;

    public ProvidersRepositaryImpl(ProvidersRESTService providersService, ProvidersCashe providersCashe) {
        this.providersService = providersService;
        this.providersCashe = providersCashe;
    }

    public LiveData<ProvidersWrapper> getProviders() {
        LiveData<ProvidersWrapper> res = providersCashe.getProviders();
        if (res != null) {
            return res;
        } else {
            return getProvidersFromNetwork();
        }
    }

    public LiveData<ProvidersWrapper> getProvidersFromNetwork() {
        final ProvidersWrapper providersWrapper = new ProvidersWrapper();
        final MutableLiveData<ProvidersWrapper> res = new MutableLiveData<>();
        res.setValue(providersWrapper);
        providersService.getProviders().enqueue(new Callback<ProvidersResponse>() {
            @Override
            public void onResponse(Call<ProvidersResponse> call, Response<ProvidersResponse> response) {
                providersWrapper.setProviders(response.body().getProviders());
                providersWrapper.setStatus(ProvidersWrapper.Status.DONE);
                res.setValue(providersWrapper);
                providersCashe.addProviders(response.body());
            }

            @Override
            public void onFailure(Call<ProvidersResponse> call, Throwable t) {
                providersWrapper.setStatus(ProvidersWrapper.Status.ERROR);
                res.setValue(providersWrapper);
            }
        });
        return res;
    }
}
