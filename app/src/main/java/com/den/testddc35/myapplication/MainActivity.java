package com.den.testddc35.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, new ProvidersFragment()).commit();
    }
}
