package com.den.testddc35.myapplication;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.den.testddc35.myapplication.adapters.ProvidersAdapter;
import com.den.testddc35.myapplication.adapters.ProvidersAdapter2;
import com.den.testddc35.myapplication.adapters.ProvidersAdapterAbstract;
import com.den.testddc35.myapplication.data.Provider;
import com.den.testddc35.myapplication.data.ProvidersWrapper;
import com.den.testddc35.myapplication.databinding.FragmentProvidersBinding;

import java.util.List;

public class ProvidersFragment extends Fragment {
    public static final String KEY_VIEW = "view";

    private ProvidersViewModel providersViewModel;
    private FragmentProvidersBinding binding;
    private ProvidersAdapterAbstract providersAdapter;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_providers, container, false);
        View view = binding.getRoot();
        providersViewModel = ViewModelProviders.of(getActivity()).get(ProvidersViewModel.class);
        setupRecyclerView(view);
        view.findViewById(R.id.refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadProviders();
            }
        });
        loadProviders();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_providers, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.view) {
            changeViewMode();
            changeAdapter();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadProviders() {
        binding.setStatus(ProvidersWrapper.Status.DOWNLOADING);
        providersViewModel.getProviders().observe(this, new Observer<ProvidersWrapper>() {
            @Override
            public void onChanged(@Nullable ProvidersWrapper providersWrapper) {
                providersAdapter.setProviders(providersWrapper.getProviders());
                binding.setStatus(providersWrapper.getStatus());
            }
        });
    }

    private void setupRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        providersAdapter = getProvidersAdapter();
        recyclerView.setAdapter(providersAdapter);
    }

    private ProvidersAdapterAbstract getProvidersAdapter(){
        ProvidersAdapterAbstract res = null;
        switch (getViewMode()) {
            case 0:
                res = new ProvidersAdapter();
                break;
            case 1:
                res = new ProvidersAdapter2();
                break;
        }
        return res;
    }

    private void changeAdapter() {
        List<Provider> providers = providersAdapter.getItems();
        providersAdapter=getProvidersAdapter();
        providersAdapter.setProviders(providers);
        recyclerView.setAdapter(providersAdapter);
    }

    private int getViewMode() {
        return PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(KEY_VIEW, 0);
    }

    private void changeViewMode() {
        int mode = getViewMode();
        mode++;
        if (mode > 1)
            mode = 0;
        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt(KEY_VIEW, mode).commit();
    }
}
