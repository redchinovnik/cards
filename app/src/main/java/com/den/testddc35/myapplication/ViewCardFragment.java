package com.den.testddc35.myapplication;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.den.testddc35.myapplication.data.GiftCard;
import com.den.testddc35.myapplication.databinding.FragmentProvidersBinding;
import com.den.testddc35.myapplication.databinding.FragmentViewCardBinding;

public class ViewCardFragment extends Fragment {
    private GiftCard card;
    private FragmentViewCardBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        card = GiftCard.fromJSON(getArguments().getString(ViewCardActivity.CARD));
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_view_card, container, false);
        View view = binding.getRoot();
        binding.setCard(card);
        binding.setView(view.findViewById(R.id.dynamicBkg));
        animateDescritpyion(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String transitionName = getArguments().getString(ViewCardActivity.TRANSITION_NAME);
            View v = view.findViewById(R.id.frameLayout);
            v.setTransitionName(transitionName);
        }

        return view;
    }

    private void animateDescritpyion(View view) {
        view.findViewById(R.id.dynamicBkg).setBackgroundColor(getArguments().getInt(ViewCardActivity.COLOR));
        View desc = view.findViewById(R.id.desc);
        desc.setTranslationY(500f);
        desc.setAlpha(0f);
        desc.animate().translationY(0).setStartDelay(200).setDuration(500).setInterpolator(new AccelerateDecelerateInterpolator()).start();
        desc.animate().alpha(1f).setStartDelay(400).setDuration(300).setInterpolator(new AccelerateDecelerateInterpolator()).start();

    }
}
