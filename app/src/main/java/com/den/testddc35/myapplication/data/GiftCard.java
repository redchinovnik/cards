package com.den.testddc35.myapplication.data;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GiftCard {
    private static Gson gson=new Gson();

    public String toJSON(){
        return gson.toJson(this);
    }

    public static GiftCard fromJSON(String json){
        return gson.fromJson(json,GiftCard.class);
    }

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("featured")
    @Expose
    private Boolean featured;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("credits")
    @Expose
    private Integer credits;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("codes_count")
    @Expose
    private Integer codesCount;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("redeem_url")
    @Expose
    private String redeemUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getCodesCount() {
        return codesCount;
    }

    public void setCodesCount(Integer codesCount) {
        this.codesCount = codesCount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRedeemUrl() {
        return redeemUrl;
    }

    public void setRedeemUrl(String redeemUrl) {
        this.redeemUrl = redeemUrl;
    }

}
