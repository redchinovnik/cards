package com.den.testddc35.myapplication.data_sources;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.den.testddc35.myapplication.data.ProvidersResponse;
import com.den.testddc35.myapplication.data.ProvidersWrapper;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class ProvidersCahseImpl implements ProvidersCashe {
    private static final String FILE_CASHE = "cashe";
    private static final long EXPIRATION_TIME = 60000;//кэш сохраняется на протяжении минуты
    private static final String EXP_KEY = "exp";
    private Gson gson = new Gson();
    private Context context;

    public ProvidersCahseImpl(Context context) {
        this.context = context;
    }

    @Override
    public LiveData<ProvidersWrapper> getProviders() {
        if (hasExpiration())
            return null;
        ProvidersResponse providersResponse = null;
        try {
            Log.w("xxx", "xxx");
            File directory = context.getCacheDir();
            File file = new File(directory, FILE_CASHE);
            String s = new BufferedReader(new FileReader(file)).readLine();
            providersResponse = gson.fromJson(s, ProvidersResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (providersResponse != null) {
            final ProvidersWrapper providersWrapper = new ProvidersWrapper();
            final MutableLiveData<ProvidersWrapper> res = new MutableLiveData<>();
            providersWrapper.setStatus(ProvidersWrapper.Status.DONE);
            providersWrapper.setProviders(providersResponse.getProviders());
            res.setValue(providersWrapper);
            return res;
        } else
            return null;

        //в фоновом потоке при отсутствии или ошибке кеша надо уведомлять репозитарий для перезагрузки из сети
        /*
        new AsyncTask<Void, Void, ProvidersResponse>() {
            @Override
            protected ProvidersResponse doInBackground(Void... voids) {
                try {
                    File file = File.createTempFile(FILE_CASHE, null, context.getCacheDir());
                    ProvidersResponse providersResponse = gson.fromJson(new FileReader(file), ProvidersResponse.class);
                    return providersResponse;
                 } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(ProvidersResponse providersResponse) {

            }
        }.execute();
        return res;*/
    }

    @Override
    public void addProviders(ProvidersResponse providers) {
        try {
            File directory = context.getCacheDir();
            File file = new File(directory, FILE_CASHE);
            file.createNewFile();
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(gson.toJson(providers));
            bufferedWriter.close();
            PreferenceManager.getDefaultSharedPreferences(context).edit()
                    .putLong(EXP_KEY, System.currentTimeMillis() + EXPIRATION_TIME)
                    .commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setExpiration(long ms) {

    }

    private boolean hasExpiration() {
        long exp_time = PreferenceManager.getDefaultSharedPreferences(context)
                .getLong(EXP_KEY, 0);
        if (System.currentTimeMillis() > exp_time)
            return true;
        return false;
    }
}
