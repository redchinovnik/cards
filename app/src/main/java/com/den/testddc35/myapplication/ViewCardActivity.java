package com.den.testddc35.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.util.Log;
import android.view.Window;

import com.den.testddc35.myapplication.data.GiftCard;

public class ViewCardActivity extends AppCompatActivity {
    public static final String CARD = "card";
    public static final String TRANSITION_NAME = "t_n";
    public static final String COLOR = "color";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_template);
        ViewCardFragment fragment = new ViewCardFragment();
        fragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            supportFinishAfterTransition();
        } else
            super.onBackPressed();
    }

    public static Intent getCallIntent(Context context, GiftCard card,int color) {
        Intent intent = new Intent(context, ViewCardActivity.class);
        intent.putExtra(CARD, card.toJSON());
        intent.putExtra(COLOR,color);
        return intent;
    }
}
