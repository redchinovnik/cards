package com.den.testddc35.myapplication;

import android.app.Application;
import android.preference.PreferenceManager;

import com.den.testddc35.myapplication.di.AppComponent;
import com.den.testddc35.myapplication.di.AppModule;
import com.den.testddc35.myapplication.di.DaggerAppComponent;

public class App extends Application {
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initComponents();
    }

    private void initComponents() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this)).build();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(ProvidersFragment.KEY_VIEW, 0).commit();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
