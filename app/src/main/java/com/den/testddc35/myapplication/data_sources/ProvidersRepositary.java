package com.den.testddc35.myapplication.data_sources;

import android.arch.lifecycle.LiveData;

import com.den.testddc35.myapplication.data.ProvidersWrapper;

public interface ProvidersRepositary {
    LiveData<ProvidersWrapper> getProviders();
}
