package com.den.testddc35.myapplication.data_sources;

import com.den.testddc35.myapplication.data.ProvidersResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ProvidersRESTService {
    @GET("/files/providers.json")
    Call<ProvidersResponse> getProviders();
}
