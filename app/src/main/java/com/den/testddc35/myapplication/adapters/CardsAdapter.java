package com.den.testddc35.myapplication.adapters;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.den.testddc35.myapplication.NavigationController;
import com.den.testddc35.myapplication.R;
import com.den.testddc35.myapplication.data.GiftCard;
import com.den.testddc35.myapplication.databinding.ItemCardBinding;

import java.util.List;

public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.ViewHolder> {
    private List<GiftCard> items;

    private int getBkgColor(View view) {
        Drawable background = view.getBackground();
        int res = 0;
        if (background instanceof ColorDrawable)
            res = ((ColorDrawable) background).getColor();
        return res;
    }

    private View.OnClickListener cardClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                View v=view.findViewById(R.id.dynamicBkg);
                NavigationController.viewCard(view, (GiftCard) view.getTag(),getBkgColor(v));
            } catch (Exception ex) {

            }
        }
    };

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ItemCardBinding itemCardBinding;
        public View root;

        public ViewHolder(View v, View.OnClickListener clickListener) {
            super(v);
            root = v;
            root.setOnClickListener(clickListener);
            itemCardBinding = DataBindingUtil.bind(v);
            itemCardBinding.setView(v.findViewById(R.id.dynamicBkg));
        }
    }


    public void setCards(List<GiftCard> cards) {
        this.items = cards;
        notifyDataSetChanged();
    }

    @Override
    public CardsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemCardBinding binding = ItemCardBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot(), cardClickListener);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GiftCard card = items.get(position);
        holder.itemCardBinding.setCard(card);
        holder.root.setTag(card);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}