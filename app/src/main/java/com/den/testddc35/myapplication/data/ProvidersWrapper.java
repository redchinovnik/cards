package com.den.testddc35.myapplication.data;

import java.util.List;

public class ProvidersWrapper {
    public static enum Status {DOWNLOADING, DONE, ERROR}



    private int errorCode;
    private List<Provider> providers;
    private Status status=Status.DOWNLOADING;


    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}

