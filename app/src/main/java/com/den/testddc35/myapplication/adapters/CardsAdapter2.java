package com.den.testddc35.myapplication.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.den.testddc35.myapplication.data.GiftCard;
import com.den.testddc35.myapplication.databinding.ItemCard2Binding;

import java.util.List;

public class CardsAdapter2 extends RecyclerView.Adapter<CardsAdapter2.ViewHolder> {
    private List<GiftCard> items;


    private View.OnClickListener cardClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                Toast.makeText(view.getContext(), "STUB...", Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {

            }
        }
    };

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ItemCard2Binding itemCardBinding;
        public View root;

        public ViewHolder(View v, View.OnClickListener clickListener) {
            super(v);
            root = v;
            root.setOnClickListener(clickListener);
            itemCardBinding = DataBindingUtil.bind(v);
          }
    }


    public void setCards(List<GiftCard> cards) {
        this.items = cards;
        notifyDataSetChanged();
    }

    @Override
    public CardsAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemCard2Binding binding = ItemCard2Binding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot(), cardClickListener);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GiftCard card = items.get(position);
        holder.itemCardBinding.setCard(card);
        holder.root.setTag(card);
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}